package com.salen.salenMali;

import com.salen.salenMali.Repository.*;
import com.salen.salenMali.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;


@SpringBootApplication
public class MaliSalenApplication implements CommandLineRunner {

	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private MarieRepository marieRepository;
	@Autowired
	private MarchandRepository marchandRepository;
	@Autowired
	private RecouvrerRepository recouvrerRepository;
	@Autowired
	private RecouvreurRepository recouvreurRepository;
	@Autowired
	private  MarcheRepository marcheRepository;




	public static void main(String[] args) {
		SpringApplication.run(MaliSalenApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Commune c1= communeRepository.save(new Commune("Commune 1"));
		Commune c2=communeRepository.save(new Commune("Commune 2"));
		Commune c3=communeRepository.save(new Commune("Commune 3"));
		Commune c4=communeRepository.save(new Commune("Commune 4"));

		Marie m1=marieRepository.save(new Marie("M 1" , c1));
		Marie m2=marieRepository.save(new Marie("M 2" , c1));
		marieRepository.save(new Marie("M 3" , c1));
		marieRepository.save(new Marie("M 4" , c2));
		marieRepository.save(new Marie("M 5" , c2));
		marieRepository.save(new Marie("M 6" , c3));


		Marche marche1=marcheRepository.save(new Marche("Marche 1",m1));
		Marche marche2=marcheRepository.save(new Marche("Marche 2",m1));
		Marche marche3=marcheRepository.save(new Marche("Marche 3",m2));

		Marchand merchand1= marchandRepository.save(new Marchand("coul","fous",1,new Date() ,"78 90 00 88",marche1));
		Marchand merchand2=marchandRepository.save(new Marchand("baba","tam",2,new Date() ,"78 90 00 88",marche1));
		Marchand merchand3=marchandRepository.save(new Marchand("san","al",3,new Date() ,"78 90 00 88",marche2));
		Marchand merchand4=marchandRepository.save(new Marchand("sanimata","yattara",3,new Date() ,"78 90 000 88",marche2));


		Recouvreur r1=recouvreurRepository.save(new Recouvreur("baba","tam" ,"89 76 88 90 ","COOO1"));
		Recouvreur r2=recouvreurRepository.save(new Recouvreur("als","fous" ,"89 76 88 90 ","COOO2"));
		Recouvreur r3=recouvreurRepository.save(new Recouvreur("aminata","yattara" ,"89 76 88 90 ","COOO3"));

		recouvrerRepository.save(new Recouvrer(2000, new Date(),merchand1,r1));
		recouvrerRepository.save(new Recouvrer(5000, new Date(),merchand2,r1));
		recouvrerRepository.save(new Recouvrer(3000, new Date(),merchand3,r2));
		recouvrerRepository.save(new Recouvrer(1000, new Date(),merchand4,r3));

















	}
}

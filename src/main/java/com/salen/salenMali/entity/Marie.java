package com.salen.salenMali.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Marie implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMarie;
    @Column(length = 50)
    private String libelleMarie;

    @ManyToOne
    private Commune commune;
    @OneToMany(mappedBy = "marie")
   private Collection<Marche> marches;

    public Marie() {
    }

    public Marie(String libelleMarie, Commune commune) {
        this.libelleMarie = libelleMarie;
        this.commune = commune;
    }

    public long getIdMarie() {
        return idMarie;
    }

    public void setIdMarie(long idMarie) {
        this.idMarie = idMarie;
    }

    public String getLibelleMarie() {
        return libelleMarie;
    }

    public void setLibelleMarie(String libelleMarie) {
        this.libelleMarie = libelleMarie;
    }

    public Commune getCommune() {
        return commune;
    }

    public void setCommune(Commune commune) {
        this.commune = commune;
    }

    public Collection<Marche> getMarches() {
        return marches;
    }

    public void setMarches(Collection<Marche> marches) {
        this.marches = marches;
    }

    @Override
    public String toString() {
        return "Marie{" +
                "idMarie=" + idMarie +
                ", libelleMarie='" + libelleMarie + '\'' +
                ", commune=" + commune +
                ", marches=" + marches +
                '}';
    }
}

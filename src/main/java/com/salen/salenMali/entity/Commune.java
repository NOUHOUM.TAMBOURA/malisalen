package com.salen.salenMali.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Commune implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCommune;
    @Column(length = 50)
    private String libelleCommune;

    @OneToMany(mappedBy = "commune")
    private Collection<Marie> maries;

    public long getIdCommune() {
        return idCommune;
    }

    public void setIdCommune(long idCommune) {
        this.idCommune = idCommune;
    }

    public String getLibelleCommune() {
        return libelleCommune;
    }

    public void setLibelleCommune(String libelleCommune) {
        this.libelleCommune = libelleCommune;
    }

    public Commune() {
    }

    public Commune(String libelleCommune) {
        this.libelleCommune = libelleCommune;
    }

    @Override
    public String toString() {
        return "Commune{" +
                "idCommune=" + idCommune +
                ", libelleCommune='" + libelleCommune + '\'' +
                '}';
    }
}

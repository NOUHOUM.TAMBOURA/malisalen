package com.salen.salenMali.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Marche implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMarche;
    @Column(length = 50)
    private String libelleMarche;

    @OneToMany(mappedBy = "marche")
    private Collection<Marchand> marchands;

    @ManyToOne
    private Marie marie;

    public Marche() {
    }

    public Marche(String libelleMarche, Marie marie) {
        this.libelleMarche = libelleMarche;
        this.marie = marie;
    }

    public long getIdMarche() {
        return idMarche;
    }

    public void setIdMarche(long idMarche) {
        this.idMarche = idMarche;
    }

    public String getLibelleMarche() {
        return libelleMarche;
    }

    public void setLibelleMarche(String libelleMarche) {
        this.libelleMarche = libelleMarche;
    }

    public Collection<Marchand> getMarchands() {
        return marchands;
    }

    public void setMarchands(Collection<Marchand> marchands) {
        this.marchands = marchands;
    }

    public Marie getMarie() {
        return marie;
    }

    public void setMarie(Marie marie) {
        this.marie = marie;
    }

    @Override
    public String toString() {
        return "Marche{" +
                "idMarche=" + idMarche +
                ", libelleMarche='" + libelleMarche + '\'' +
                ", marchands=" + marchands +
                ", marie=" + marie +
                '}';
    }
}

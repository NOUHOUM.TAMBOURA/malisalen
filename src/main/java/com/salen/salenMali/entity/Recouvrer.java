package com.salen.salenMali.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Recouvrer implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long idRecouvrer;
  private double montantpaye;
  private Date datePayement;

  @ManyToOne
  private Marchand marchand;
  @ManyToOne
  private Recouvreur recouvreur;

  public Recouvrer() {

  }

  public Recouvrer(double montantpaye, Date datePayement, Marchand marchand, Recouvreur recouvreur) {
    this.montantpaye = montantpaye;
    this.datePayement = datePayement;
    this.marchand = marchand;
    this.recouvreur = recouvreur;
  }

  public long getIdRecouvrer() {
    return idRecouvrer;
  }

  public void setIdRecouvrer(long idRecouvrer) {
    this.idRecouvrer = idRecouvrer;
  }

  public double getMontantpaye() {
    return montantpaye;
  }

  public void setMontantpaye(double montantpaye) {
    this.montantpaye = montantpaye;
  }

  public Date getDatePayement() {
    return datePayement;
  }

  public void setDatePayement(Date datePayement) {
    this.datePayement = datePayement;
  }

  public Marchand getMarchand() {
    return marchand;
  }

  public void setMarchand(Marchand marchand) {
    this.marchand = marchand;
  }

  public Recouvreur getRecouvreur() {
    return recouvreur;
  }

  public void setRecouvreur(Recouvreur recouvreur) {
    this.recouvreur = recouvreur;
  }

  @Override
  public String toString() {
    return "Recouvrer{" +
            "idRecouvrer=" + idRecouvrer +
            ", montantpaye=" + montantpaye +
            ", datePayement=" + datePayement +
            ", marchand=" + marchand +
            ", recouvreur=" + recouvreur +
            '}';
  }
}

package com.salen.salenMali.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
public class Marchand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idMarchand;
    @Column(length = 50)
    private String nom;
    @Column(length = 50)
    private String prenom;
    private int numeroplace;
    private Date dateOccupation;
    @Column(length = 50)
    private String telephone;

    @ManyToOne
    private  Marche marche;

    @OneToMany(mappedBy = "marchand")
    //@JsonProperty(access = JsonProperty.Access.READ_WRITE)
    private Collection<Recouvrer> recouvrers;

    public Marchand() {
    }

    public Marchand(String nom, String prenom, int numeroplace, Date dateOccupation, String telephone, Marche marche) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroplace = numeroplace;
        this.dateOccupation = dateOccupation;
        this.telephone = telephone;
        this.marche = marche;
    }

    public long getIdMarchand() {
        return idMarchand;
    }

    public void setIdMarchand(long idMarchand) {
        this.idMarchand = idMarchand;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getNumeroplace() {
        return numeroplace;
    }

    public void setNumeroplace(int numeroplace) {
        this.numeroplace = numeroplace;
    }

    public Date getDateOccupation() {
        return dateOccupation;
    }

    public void setDateOccupation(Date dateOccupation) {
        this.dateOccupation = dateOccupation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Marche getMarche() {
        return marche;
    }

    public void setMarche(Marche marche) {
        this.marche = marche;
    }

    public Collection<Recouvrer> getRecouvrers() {
        return recouvrers;
    }

    public void setRecouvrers(Collection<Recouvrer> recouvrers) {
        this.recouvrers = recouvrers;
    }

    @Override
    public String toString() {
        return "Marchand{" +
                "idMarchand=" + idMarchand +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", numeroplace=" + numeroplace +
                ", dateOccupation=" + dateOccupation +
                ", telephone='" + telephone + '\'' +
                ", marche=" + marche +
                ", recouvrers=" + recouvrers +
                '}';
    }
}

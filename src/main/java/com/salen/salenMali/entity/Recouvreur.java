package com.salen.salenMali.entity;



import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Recouvreur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRecouvreur;
    @Column(length = 50)
    private  String nom;
    @Column(length = 50)
    private  String prenom;
    @Column(length = 50)
    private String telephone;
    private String codeRecouvreur;

    @OneToMany(mappedBy = "recouvreur")
    private Collection<Recouvrer> recouvrers;

    public Recouvreur() {
    }

    public long getIdRecouvreur() {
        return idRecouvreur;
    }

    public void setIdRecouvreur(long idRecouvreur) {
        this.idRecouvreur = idRecouvreur;
    }

    public Collection<Recouvrer> getRecouvrers() {
        return recouvrers;
    }

    public void setRecouvrers(Collection<Recouvrer> recouvrers) {
        this.recouvrers = recouvrers;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCodeRecouvreur() {
        return codeRecouvreur;
    }

    public void setCodeRecouvreur(String codeRecouvreur) {
        this.codeRecouvreur = codeRecouvreur;
    }

    public Recouvreur(String nom, String prenom, String telephone, String codeRecouvreur) {
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.codeRecouvreur = codeRecouvreur;
    }

    @Override
    public String toString() {
        return "Recouvreur{" +
                "idRecouvreur=" + idRecouvreur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", telephone='" + telephone + '\'' +
                ", codeRecouvreur='" + codeRecouvreur + '\'' +
                ", recouvrers=" + recouvrers +
                '}';
    }
}

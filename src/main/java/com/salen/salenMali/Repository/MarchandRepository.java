package com.salen.salenMali.Repository;

import com.salen.salenMali.entity.Marchand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin("*")
public interface MarchandRepository extends JpaRepository<Marchand, Long> {
    @RestResource(path = "/ByNom")
    public Page<Marchand> findByNom(@Param("nom") String nom , Pageable pageable);
}

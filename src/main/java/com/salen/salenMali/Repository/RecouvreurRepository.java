package com.salen.salenMali.Repository;


import com.salen.salenMali.entity.Recouvreur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")
public interface RecouvreurRepository extends JpaRepository<Recouvreur, Long> {
}

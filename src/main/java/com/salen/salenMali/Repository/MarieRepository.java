package com.salen.salenMali.Repository;

import com.salen.salenMali.entity.Marie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")
public interface MarieRepository extends JpaRepository<Marie, Long> {
}

package com.salen.salenMali.Repository;

import com.salen.salenMali.entity.Commune;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestResource
@CrossOrigin("*")
public interface CommuneRepository extends JpaRepository<Commune , Long> {

}
